library identifier: "jenkins-shared-library@main", retriever: modernSCM(
    [
        $class: "GitSCMSource",
        remote: "https://gitlab.com/student286/jenkins-shared-library.git",
        credentialsId: "GitLab-Credentials"
    ]
)

pipeline {
    agent any

    tools {
        maven "maven-3.9"
    }


    stages {

        stage("increase version") {
            steps {
                script {
                    echo "increasing app version..."
                    sh "mvn build-helper:parse-version versions:set \
                        -DnewVersion=\\\${parsedVersion.majorVersion}.\\\${parsedVersion.minorVersion}.\\\${parsedVersion.nextIncrementalVersion} \
                        versions:commit"

                    def matcher = readFile("pom.xml") =~ "<version>(.+)</version>"
                    def version = matcher[0][1]
                    env.IMAGE_NAME = "pprasant/demo-app:java-app-$version"
                }
            }

        }

        stage("build jar") {
            steps {
                script {
                    echo "building application jar..."
                    buildJar()
                }
            }
        }

        stage("build and push image") {
            steps {
                script {
                    echo "building and pushing docker image..."
                    buildImage(env.IMAGE_NAME)
                    dockerLogin()
                    dockerPush(env.IMAGE_NAME)
                }
            }
        }

        // stage("deploy") {
        //     steps {
        //         script {
        //             echo "deploying docker image to EC2..."
        //             def dockerCMD = "docker run -d -p 8080:8080 ${IMAGE_NAME}"
        //             sshagent(["EC2-Server-Key"]) {
        //                 sh "ssh -o StrictHostKeyChecking=no ec2-user@18.215.238.169 ${dockerCMD}"
        //             }
        //         }
        //     }
        // }

        // stage("deploy") {
        //     steps {
        //         script {
        //             echo "deploying docker image to EC2..."
        //             def dockerComposeCMD = "docker-compose -f docker-compose.yaml up --detach"
        //             sshagent(["EC2-Server-Key"]) {
        //                 sh "scp docker-compose.yaml ec2-user@18.215.238.169:/home/ec2-user"
        //                 sh "ssh -o StrictHostKeyChecking=no ec2-user@18.215.238.169 ${dockerComposeCMD}"
        //             }
        //         }
        //     }
        // }

        stage("deploy") {
            steps {
                script {
                    echo "deploying docker image to EC2..."
                    
                    def shellCMD = "bash ./server-cmds.sh ${env.IMAGE_NAME}"

                    def ec2Instance = "ec2-user@18.215.238.169"

                    sshagent(["EC2-Server-Key"]) {
                        sh "scp server-cmds.sh ${ec2Instance}:/home/ec2-user"
                        sh "scp docker-compose.yaml ${ec2Instance}:/home/ec2-user"
                        sh "ssh -o StrictHostKeyChecking=no ${ec2Instance} ${shellCMD}"
                    }
                }
            }
        }

        stage("commit version update") {
            steps {

                script {

                    echo "Commit updated verion to git..."
                    withCredentials([usernamePassword(credentialsId: "GitLab-Credentials", passwordVariable: "PASS", usernameVariable: "USER")]) {
                        
                        // this can be set only once
                        sh 'git config --global user.email "jenkins@mail.com"'
                        sh 'git config --global user.name "jenkins"'

                        sh "git remote set-url origin https://${USER}:${PASS}@gitlab.com/student286/java-maven-jenkins-complete-cicd.git"
                        sh "git add ."
                        sh 'git commit -m "version update from jenkins"'
                        sh "git push origin HEAD:main"
                    }

                }
            }
        }

    }
}
