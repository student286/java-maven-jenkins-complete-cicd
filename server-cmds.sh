#!/bin/bash

export EXPORT_IMAGE_NAME=$1

export DOCKER_USERNAME=$2

export DOCKER_PASSWD=$3

echo $DOCKER_PASSWD | docker login -u $DOCKER_USERNAME --password-stdin
docker-compose -f docker-compose.yaml up --detach




