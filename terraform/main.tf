terraform {
  required_version = ">= 0.12"
  backend "s3" {
    bucket = "java-maven-app-bucket-123"
    key = "myapp/state.tfstate"
    region = "us-east-2"
  }
}

provider "aws" {
  region  = "us-east-2"
}

resource "aws_vpc" "my_vpc" {
  cidr_block = var.vpc_cidr_block
  tags = {
    "Name" = "${var.env_prefix}-vpc"
  }

}

resource "aws_subnet" "subnet_1" {
  vpc_id            = aws_vpc.my_vpc.id
  cidr_block        = var.subnet_cidr_block
  availability_zone = var.avail_zone
  tags = {
    "Name" = "${var.env_prefix}-subnet-1"
  }
}

resource "aws_internet_gateway" "my_igw" {
  vpc_id = aws_vpc.my_vpc.id
  tags = {
    "Name" = "${var.env_prefix}-igw"
  }
}


resource "aws_default_route_table" "default_rtb" {
  default_route_table_id = aws_vpc.my_vpc.default_route_table_id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.my_igw.id
  }
  tags = {
    "Name" = "${var.env_prefix}-default-rtb"
  }
}

resource "aws_default_security_group" "default_sg" {
  vpc_id = aws_vpc.my_vpc.id

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = [var.my_ip]
  }

  ingress {
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
    prefix_list_ids = []
  }

  tags = {
    "Name" = "${var.env_prefix}-default-sg"
  }

}

data "aws_ami" "ami" {
  owners      = ["amazon"]
  most_recent = true
  filter {
    name   = "name"
    values = ["amzn2-ami-hvm-*-x86_64-gp2"]
  }

}



resource "aws_instance" "my_app_server" {
  ami                         = data.aws_ami.ami.id
  instance_type               = var.instance_type
  subnet_id                   = aws_subnet.subnet_1.id
  vpc_security_group_ids      = [aws_default_security_group.default_sg.id]
  availability_zone           = var.avail_zone
  associate_public_ip_address = true
  key_name                    = "hello"

  user_data = file("install.sh")

  tags = {
    "Name" = "${var.env_prefix}-server"
  }

}


output "server_public_ip" {
  value = aws_instance.my_app_server.public_ip
}


