// def buildApp() {
//     echo "building application.."
// }

// def testApp() {
//     echo "testing application.."
// }

// def deployApp() {
//     echo "Deploying the application..."
//     echo "Deploying version ${params.VERSION}"
// }


def buildJar() {
    echo "Building the application..."
    sh "mvn package"
}

def buildImage() {
    echo "Building docker image..."
    withCredentials([usernamePassword(credentialsId: "dockerhub-credentials", passwordVariable: "PASS", usernameVariable: "USER")]) {
        sh "docker build -t pprasant/demo-app:java-app-2.0 ."
        sh "echo $PASS | docker login -u $USER --password-stdin"
        sh "docker push pprasant/demo-app:java-app-2.0"
    }
}



return this