library identifier: "jenkins-shared-library@main", retriever: modernSCM(
    [
        $class: "GitSCMSource",
        remote: "https://gitlab.com/student286/jenkins-shared-library.git",
        credentialsId: "GitLab-Credentials"
    ]
)

pipeline {
    agent any

    tools {
        maven "maven-3.9"
    }


    stages {

        stage("increase version") {
            steps {
                script {
                    echo "increasing app version..."
                    sh "mvn build-helper:parse-version versions:set \
                        -DnewVersion=\\\${parsedVersion.majorVersion}.\\\${parsedVersion.minorVersion}.\\\${parsedVersion.nextIncrementalVersion} \
                        versions:commit"

                    def matcher = readFile("pom.xml") =~ "<version>(.+)</version>"
                    def version = matcher[0][1]
                    env.IMAGE_NAME = "pprasant/demo-app:java-app-$version"
                }
            }

        }

        stage("build jar") {
            steps {
                script {
                    echo "building application jar..."
                    buildJar()
                }
            }
        }

        stage("build and push image") {
            steps {
                script {
                    echo "building and pushing docker image..."
                    buildImage(env.IMAGE_NAME)
                    dockerLogin()
                    dockerPush(env.IMAGE_NAME)
                }
            }
        }

        stage("provision server") {
            environment {
                AWS_ACCESS_KEY_ID = credentials("JENKINS_AWS_ACCESS_KEY_ID")
                AWS_SECRET_ACCESS_KEY = credentials("JENKINS_AWS_SECRET_ACCESS_KEY")
                TF_VAR_env_prefix = "prod"
            }

            steps {
                script {
                    dir("terraform") {
                        sh "terraform init"
                        sh "terraform apply --auto-approve"
                        EC2_PUBLIC_IP = sh(
                                script: "terraform output server_public_ip",
                                returnStdout: true
                            ).trim()
                    }
                }
            }
        }

        stage("deploy") {
            environment {
                DOCKER_CREDENTIALS = credentials("dockerhub-credentials")
            }
            steps {
                script {

                    echo "waiting for server to initialize..."

                    sleep(time: 60, unit: "SECONDS")

                    echo "deploying docker image to EC2..."
                    
                    def shellCMD = "bash ./server-cmds.sh ${env.IMAGE_NAME} ${DOCKER_CREDENTIALS_USR} ${DOCKER_CREDENTIALS_PSW}"

                    def ec2Instance = "ec2-user@${EC2_PUBLIC_IP}"

                    sshagent(["terraform-server-key"]) {
                        sh "scp -o StrictHostKeyChecking=no server-cmds.sh ${ec2Instance}:/home/ec2-user"
                        sh "scp -o StrictHostKeyChecking=no docker-compose.yaml ${ec2Instance}:/home/ec2-user"
                        sh "ssh -o StrictHostKeyChecking=no ${ec2Instance} ${shellCMD}"
                    }
                }
            }
        }

        stage("commit version update") {
            steps {

                script {

                    echo "Commit updated verion to git..."
                    withCredentials([usernamePassword(credentialsId: "GitLab-Credentials", passwordVariable: "PASS", usernameVariable: "USER")]) {
                        
                        // this can be set only once
                        sh 'git config --global user.email "jenkins@mail.com"'
                        sh 'git config --global user.name "jenkins"'

                        sh "git remote set-url origin https://${USER}:${PASS}@gitlab.com/student286/java-maven-jenkins-complete-cicd.git"
                        sh "git add ."
                        sh 'git commit -m "version update from jenkins"'
                        sh "git push origin HEAD:main"
                    }

                }
            }
        }

    }
}
